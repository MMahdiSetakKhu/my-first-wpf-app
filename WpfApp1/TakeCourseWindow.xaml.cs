﻿using System.Data;
using System.Linq;
using System.Windows;
using MySql.Data.MySqlClient;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for TakeCourseWindow.xaml
    /// </summary>
    public partial class TakeCourseWindow
    {
        private int _stdId;
        public TakeCourseWindow(int stdId)
        {
            _stdId = stdId;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            RefreshAvailable();
            RefreshTaken();
        }

        void RefreshTaken()
        {
            DataView dataView = App.Context.ExecuteQueryDefaultView($"select ID, name, capacity from take join course on ID=course_id where std_id={_stdId};");
            dgTaken.ItemsSource = dataView;
        }

        void RefreshAvailable()
        {
            DataView dataView = App.Context.ExecuteQueryDefaultView($"select * from course where ID not in (select ID from take join course on ID=course_id where std_id={_stdId});");
            dgAvailable.ItemsSource = dataView;
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);

            currentWindow?.Hide();
            new StudentWindow(_stdId).Show();
            currentWindow?.Close();
        }

        private void btnDrop_Click(object sender, RoutedEventArgs e)
        {
            //todo exception handling not selected and empty selection
            object[] selectedItem = ((System.Data.DataRowView)dgTaken.SelectedItem).Row.ItemArray;
            App.Context.ExecuteNonQuery($"delete from take where std_id={_stdId} and course_id={selectedItem[0]};");
            App.Context.ExecuteNonQuery($"delete from mark where std_id={_stdId} and course_id={selectedItem[0]};");
            RefreshAvailable();
            RefreshTaken();
        }

        private void btnTake_Click(object sender, RoutedEventArgs e)
        {
            //todo exception handling not selected and empty selection
            object[] selectedItem = ((System.Data.DataRowView)dgAvailable.SelectedItem).Row.ItemArray;
            App.Context.ExecuteNonQuery($"insert into take values ({_stdId}, {selectedItem[0]});");
            App.Context.ExecuteNonQuery($"insert into mark values ({_stdId}, {selectedItem[0]}, 0);");
            RefreshAvailable();
            RefreshTaken();
        }
    }
}
