﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;

namespace WpfApp1
{
    public class DbContext
    {
        private readonly MySqlConnection _connection;

        public DbContext(MySqlConnection connection)
        {
            _connection = connection;
        }

        public DataView ExecuteQueryDefaultView(string query)
        {
            Open();
            MySqlDataReader dataReader =
                new MySqlCommand {CommandText = query, Connection = _connection}.ExecuteReader();
            DataTable result = new DataTable();
            result.Load(dataReader);
            Close();
            return result.DefaultView;
        }

        public List<List<object>> ExecuteQuery(string query)
        {
            Open();

            MySqlDataReader dataReader =
                new MySqlCommand {CommandText = query, Connection = _connection}.ExecuteReader();

            List<List<object>> data = new List<List<object>>();

            while (dataReader.Read())
            {
                var row = new object[dataReader.FieldCount];
                dataReader.GetValues(row);
                var test = dataReader.GetName(0);

                data.Add(row.ToList());
            }

            Close();

            return data;
        }

        public int ExecuteNonQuery(string query)
        {
            Open();

            int affectedRows = new MySqlCommand {CommandText = query, Connection = _connection}.ExecuteNonQuery();

            Close();

            return affectedRows;
        }

        private void Open()
        {
            _connection.Open();
        }

        private void Close()
        {
            _connection.Close();
        }

        public bool StudentSignIn(int id, string password)
        {
            List<List<object>> result =
                ExecuteQuery("select * from student where id=" + id + " and password='" + password + "'");
            return result.Count > 0;
        }

        public bool ProfessorSignIn(int id, string password)
        {
            List<List<object>> result =
                ExecuteQuery("select * from professor where id=" + id + " and password='" + password + "'");
            return result.Count > 0;
        }
    }
}