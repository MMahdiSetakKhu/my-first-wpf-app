﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for ProfessorWindow.xaml
    /// </summary>
    public partial class ProfessorWindow
    {
        private int _profId;
        private List<List<object>> courses;

        public ProfessorWindow(int profId)
        {
            _profId = profId;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            List<List<object>> profile = App.Context.ExecuteQuery($"select * from professor where id={profId};");
            txtName.Text = profile[0][1].ToString();
            txtID.Text = profile[0][0].ToString();
            txtDepartment.Text = profile[0][3].ToString();
            List<List<object>> students = App.Context.ExecuteQuery("select count(ID) from student;");
            txtSudents.Text = students[0][0].ToString();
            courses = App.Context.ExecuteQuery("select count(ID) from course;");
            txtCourses.Text = courses[0][0].ToString();

            cmbCourses.Items.Clear();
            courses = App.Context.ExecuteQuery("select ID, name from course;");
            foreach (List<object> course in courses)
            {
                cmbCourses.Items.Add(course[1]);
            }

            cmbCourses.SelectedIndex = 0;
        }

        private void btnSignOut_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);

            currentWindow?.Hide();
            new SignInWindow().Show();
            currentWindow?.Close();
        }

        private void btnCreateCourse_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new CreateCourseWindow(_profId).Show();
            currentWindow?.Close();
        }

        private void btnScoring_Click(object sender, RoutedEventArgs e)
        {
            int index = cmbCourses.SelectedIndex;
            int course_id = (int) courses[index][0];
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new CourseStudent(_profId, course_id).Show();
            currentWindow?.Close();
        }
    }
}