﻿using System;
using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for SignInWindow.xaml
    /// </summary>
    public partial class SignInWindow
    {
        public SignInWindow()
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            bool error = false;
            int id = 0;
            try
            {
                id = int.Parse(txtId.Text);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                error = true;
            }

            string password = PasswordBox.Password;
            if (ToggleSwitch.IsOn)
            {
                bool successful = App.Context.ProfessorSignIn(id, password);
                if (!successful)
                {
                    error = true;
                }
                else
                {
                    var currentWindow = Application.Current.Windows.OfType<Window>()
                        .SingleOrDefault(w => w.IsActive);
                    currentWindow?.Hide();
                    new ProfessorWindow(id).Show();
                    currentWindow?.Close();
                }
            }
            else
            {
                bool successful = App.Context.StudentSignIn(id, password);
                if (!successful)
                {
                    error = true;
                }
                else
                {
                    var currentWindow = Application.Current.Windows.OfType<Window>()
                        .SingleOrDefault(w => w.IsActive);
                    currentWindow?.Hide();
                    new StudentWindow(id).Show();
                    currentWindow?.Close();
                }
            }

            if (error)
                txtError.Text = "Wrong ID or Password";
        }
    }
}