﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private string _connectionString = "";
        public MainWindow()
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private void btnSingUp_Click(object sender, RoutedEventArgs e)
        {
            _connectionString = "server=127.0.0.1;database=DBThirdProject;uid=" + txtUser.Text + ";password='" + txtPassword + "';";
            App.Context = new DbContext(new MySqlConnection(_connectionString));

            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new SignUpWindow().Show();
            currentWindow?.Close();
        }

        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            _connectionString = "server=127.0.0.1;database=DBThirdProject;uid=" + txtUser.Text + ";password='" + txtPassword.Password + "';";
            App.Context = new DbContext(new MySqlConnection(_connectionString));

            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new SignInWindow().Show();
            currentWindow?.Close();
        }
    }
}
