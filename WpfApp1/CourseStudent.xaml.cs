﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for CourseStudent.xaml
    /// </summary>
    public partial class CourseStudent
    {
        private int _courseId;
        private int _profId;
        private string _courseName;

        public CourseStudent(int profId, int courseId)
        {
            _courseId = courseId;
            _profId = profId;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            List<List<object>> course = App.Context.ExecuteQuery($"select * from course where ID={courseId};");
            txtCourse.Text = course[0][1].ToString();
            _courseName = course[0][1].ToString();

            dgCourse.ItemsSource = App.Context.ExecuteQueryDefaultView(
                $"select std_id as ID, name, mark from (select mark.std_id, mark.course_id, mark from take join mark on take.course_id=mark.course_id and take.std_id=mark.std_id) as c join student on c.std_id=ID where course_id={courseId};");
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new ProfessorWindow(_profId).Show();
            currentWindow?.Close();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            object[] selectedItem = ((System.Data.DataRowView) dgCourse.SelectedItem).Row.ItemArray;
            int stdId = (int) selectedItem[0];
            string stdName = (string) selectedItem[1];
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new EditMarkWindow(stdId, _courseId, _profId, stdName, _courseName).Show();
            currentWindow?.Close();
        }
    }
}