﻿using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for CreateCourseWindow.xaml
    /// </summary>
    public partial class CreateCourseWindow
    {
        private readonly int _profId;
        public CreateCourseWindow(int profId)
        {
            _profId = profId;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            App.Context.ExecuteNonQuery($"insert into course values({txbId.Text}, '{txbName.Text}', {txbCapacity.Text});");
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new ProfessorWindow(_profId).Show();
            currentWindow?.Close();
        }
    }
}
