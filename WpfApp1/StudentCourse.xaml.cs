﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for StudentCourse.xaml
    /// </summary>
    public partial class StudentCourse
    {
        private int _stdId;

        public StudentCourse(int stdId)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            _stdId = stdId;
            dgCourse.ItemsSource = App.Context.ExecuteQueryDefaultView(
                $"select ID, name, mark from (select ID, name, std_id from take join course on ID=course_id where std_id={stdId}) as c join mark on c.ID=mark.course_id and c.std_id=mark.std_id;");
            List<List<object>> avg = App.Context.ExecuteQuery(
                $"select avg(m.mark) from (select ID, name, mark from (select ID, name, std_id from take join course on ID=course_id where std_id={stdId}) as c join mark on c.ID=mark.course_id and c.std_id=mark.std_id) as m;");
            txtAvg.Text = Math.Round(double.Parse(avg[0][0].ToString()), 2).ToString();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);

            currentWindow?.Hide();
            new StudentWindow(_stdId).Show();
            currentWindow?.Close();
        }
    }
}