﻿using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for EditMarkWindow.xaml
    /// </summary>
    public partial class EditMarkWindow
    {
        private int _stdId, _courseId, _profId;
        public EditMarkWindow(int stdId, int courseId, int profId, string stdName, string courseName)
        {
            _stdId = stdId;
            _courseId = courseId;
            _profId = profId;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            txtEdit.Text = $"{stdName} in {courseName}:";
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            App.Context.ExecuteNonQuery(
                $"update mark set mark={txbEdit.Text} where course_id={_courseId} and std_id={_stdId};");
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);
            currentWindow?.Hide();
            new CourseStudent(_profId, _courseId).Show();
            currentWindow?.Close();
        }
    }
}
