﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for StudentWindow.xaml
    /// </summary>
    public partial class StudentWindow
    {
        private int _stdId;

        public StudentWindow(int stdId)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            _stdId = stdId;
            List<List<object>> profile = App.Context.ExecuteQuery("select * from student where id=" + stdId + ";");
            List<List<object>> avg =
                App.Context.ExecuteQuery(
                    $"select avg(m.mark) from (select ID, name, mark from (select ID, name, std_id from take join course on ID=course_id where std_id={stdId}) as c join mark on c.ID=mark.course_id and c.std_id=mark.std_id) as m;");
            List<List<object>> courses =
                App.Context.ExecuteQuery("select count(std_id) from take where std_id=" + stdId + ";");
            txtID.Text = stdId.ToString();
            txtName.Text = (string) profile[0][1];
            txtMajor.Text = (string) profile[0][3];
            txtAvg.Text = Math.Round(double.Parse(avg[0][0].ToString()), 2).ToString();
            txtCourses.Text = courses[0][0].ToString();
        }

        private void btnSignOut_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);

            currentWindow?.Hide();
            new SignInWindow().Show();
            currentWindow?.Close();
        }

        private void btnTake_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);

            currentWindow?.Hide();
            new TakeCourseWindow(_stdId).Show();
            currentWindow?.Close();
        }

        private void btnCourse_Click(object sender, RoutedEventArgs e)
        {
            var currentWindow = Application.Current.Windows.OfType<Window>()
                .SingleOrDefault(w => w.IsActive);

            currentWindow?.Hide();
            new StudentCourse(_stdId).Show();
            currentWindow?.Close();
        }
    }
}