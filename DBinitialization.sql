drop database DBThirdProject;
create database DBThirdProject;
use DBThirdProject;

create table student (
	ID int primary key,
    name varchar(20),
    password varchar(20),
    major varchar(20)
);

insert into student values(1, "Ali Gholami", "123", "Computer Engineering");
insert into student values(2, "Amir Kabiri", "123", "Computer Engineering");
insert into student values(3, "Reza Gohari", "123", "Computer Engineering");
insert into student values(4, "Saeid Akbari", "123", "Computer Engineering");
insert into student values(5, "Fatemeh Azari", "123", "Computer Engineering");
insert into student values(6, "Arman Mansoury", "123", "Computer Engineering");
insert into student values(7, "Mohammad Hasani", "123", "Computer Engineering");
insert into student values(8, "Hasan Mohammadi", "123", "Computer Engineering");
insert into student values(9, "Reza Amini", "123", "Computer Engineering");
insert into student values(10, "Amin Rezaei", "123", "Computer Engineering");
insert into student values(11, "Zahra Asghari", "123", "Computer Engineering");
insert into student values(12, "Maryam Taghipour", "123", "Computer Engineering");
insert into student values(13, "Ladan Akbari", "123", "Computer Engineering");

create table professor (
	ID int primary key,
    name varchar(20),
    password varchar(20),
    Department varchar(20)
);

insert into professor values(1, "Dr.Reza Akbari", "123", "Computer Engineering");

create table course (
	ID int primary key,
    name varchar(20),
    capacity int
);

insert into course values(1, "Advanced Programming", 20);
insert into course values(2, "Database", 30);
insert into course values(3, "Linear Aljebra", 20);
insert into course values(4, "Math", 40);
insert into course values(5, "physics", 40);

create table take (
	std_id int,
    course_id int,
    foreign key (std_id) references student (ID),
    foreign key (course_id) references course (ID),
    primary key (std_id, course_id)
);

create table mark (
	std_id int,
    course_id int,
    mark int,
    foreign key (std_id) references student (ID),
    foreign key (course_id) references course (ID)
);

insert into take values(1, 1);
insert into take values(1, 2);
insert into take values(1, 3);
insert into take values(2, 1);
insert into take values(12, 2);
insert into take values(8, 3);
insert into take values(7, 4);
insert into take values(6, 5);
insert into take values(7, 1);
insert into take values(10, 2);
insert into take values(4, 4);
insert into take values(2, 5);
insert into take values(3, 1);
insert into take values(5, 2);
insert into take values(5, 3);
insert into take values(6, 4);
insert into take values(7, 5);
insert into take values(8, 1);
insert into take values(11, 3);
insert into take values(10, 4);
insert into take values(9, 5);
insert into take values(5, 1);



insert into mark values(1, 1, 20);
insert into mark values(1, 2, 19);
insert into mark values(1, 3, 15);
insert into mark values(2, 1, 16);
insert into mark values(12, 2, 13);
insert into mark values(8, 3, 18);
insert into mark values(7, 4, 9.75);
insert into mark values(6, 5, 17.5);
insert into mark values(7, 1, 16.75);
insert into mark values(10, 2, 17);
insert into mark values(4, 4, 20);
insert into mark values(2, 5, 19.5);
insert into mark values(3, 1, 18);
insert into mark values(5, 2, 18.5);
insert into mark values(5, 3, 16);
insert into mark values(6, 4, 17);
insert into mark values(7, 5, 16.75);
insert into mark values(8, 1, 20);
insert into mark values(11, 3, 16.5);
insert into mark values(10, 4, 17.75);
insert into mark values(9, 5, 18.25);
insert into mark values(5, 1, 19.25);




